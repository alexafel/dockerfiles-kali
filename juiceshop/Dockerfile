FROM node:16 as installer

# Labels
LABEL maintainer="Felix Alexa <alexafel@hs-albsig.de>"
LABEL version="Abgabe"

# Dependency Instance - Create Layers with node dependencies installed
ENV WORKDIR /juice-shop
WORKDIR $WORKDIR
COPY juice-shop /juice-shop
RUN npm install --production --unsafe-perm
RUN npm dedupe
RUN rm -rf frontend/node_modules

# Running Instance - Layers which contain the actual code
FROM node:16-alpine
ARG BUILD_DATE
WORKDIR /juice-shop
RUN addgroup --system --gid 1001 juicer && \
    adduser juicer --system --uid 1001 --ingroup juicer
COPY --from=installer --chown=juicer /juice-shop .
RUN mkdir logs && \
    chown -R juicer logs && \
    chgrp -R 0 ftp/ frontend/dist/ logs/ data/ i18n/ && \
    chmod -R g=u ftp/ frontend/dist/ logs/ data/ i18n/
USER 1001
EXPOSE 3000
CMD ["npm", "start"]
